<?php

class Model {

    function __construct()
    {   
    }
    
    function DataBase()
    {     
        if(empty($db))
        {
        $db = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);    
        }
        
        if ($db->connect_error)
        {
        die('Connect Error (Code:' . $mysqli->connect_errno . ') ' . $mysqli->connect_error);
        }

        return $db;
    }
}
?>