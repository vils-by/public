<?php

class View {

    function __construct()
    {
        
    }
    
    public function generate($view, $data = null)
    {       
		if(is_array($data))
        {
			extract($data);
		}	
        
        include Q_PATH.'/application/views/template.php';
    }

}
?>