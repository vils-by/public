<?php
class Route {

    function __construct()
    {
      
    }
    
    public static function Start() 
    {
            $controller_name = 'index';
            $action_name = 'index';
            $action_parameters = array();
            
        if($_SERVER['REQUEST_URI']!='/') 
        {    
            $url_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
            $uri_parts = explode('/', trim($url_path, ' /'));   
            
            if(!empty($uri_parts[0]))
            {   
            $controller_name = array_shift($uri_parts);
            } 
            
            if(!empty($uri_parts[0]))
            {
            $action_name = array_shift($uri_parts);
            }
                  
            if(count($uri_parts)%2==0)
            {
                for ($i=0; $i < count($uri_parts); $i++) 
                {
                $action_parameters[$uri_parts[$i]] = $uri_parts[++$i];
                }
            }
            else {
                Route::ErrorPage404();
            }
        }    

            $model_name = 'model_' . $controller_name;
            $controller_name = 'controller_' . $controller_name;
            $action_name = 'action_' . $action_name;
            
            
            $model_file = strtolower($model_name).'.php';
		    $model_path = Q_PATH.'/application/models/'.$model_file;
		    if(file_exists($model_path))
            {
                include $model_path;
            }
            
            $controller_file = strtolower($controller_name).'.php';
            $controller_path = Q_PATH.'/application/controllers/'.$controller_file;
            if(file_exists($controller_path))
            {
                include $controller_path;
            }
            else {
                Route::ErrorPage404();
            }

            $controller = new $controller_name();
            $action = $action_name;

            if (method_exists($controller, $action))
            {
                $controller->$action($action_parameters);
            }
            else {
                Route::ErrorPage404();                
            }
    }

    function RedirectTo($page="")
    {
        $host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        header('HTTP/1.1 301 Moved Permanently');
        header('Location:' . $host . $page);
    }

    function ErrorPage404()
    {
        $host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:' . $host . '404');
    }
}
?>