<?php

class Model_index extends Model {
    
    function __construct() 
    {
        
    }
    
    public function getAllTasks()
    {     
        $result=$this->DataBase()->query("SELECT * FROM `tasks`");
        while($row=$result->fetch_assoc())
        {
        $data[]=$row;
        }
        return $data; 
    }
    
    public function getTasksCount()
    {     
        $result=$this->DataBase()->query("SELECT COUNT(*) FROM `tasks`");
        $row = $result->fetch_row();
        return $row[0];      
    }
    
    public function getPage($activePage, $tasksOnPage, $key, $sort)
    {
        $firstTask = $tasksOnPage * ($activePage-1);
                
        $result=$this->DataBase()->query("SELECT * FROM `tasks` ORDER BY $key $sort LIMIT $firstTask, $tasksOnPage");
        while($row=$result->fetch_assoc())
        {
        $data[]=$row;
        }
        return $data; 
    }
        
    public function getTask($id)
    {       
        $result=$this->DataBase()->query("SELECT * FROM `tasks` WHERE `id`=$id");
        $data=$result->fetch_assoc();
        return $data;
    }
    
    public function Add($name, $email, $text)
    {
        $name = $this->DataBase()->real_escape_string($name);
        $email = $this->DataBase()->real_escape_string($email);
        $text = $this->DataBase()->real_escape_string($text);
        $result=$this->DataBase()->query("INSERT INTO `tasks`(`id`, `name`, `email`, `text`, `status`, `edit`) 
                                            VALUES (NULL, '$name', '$email', '$text', 0, 0)");
        if($result)
        return array('alert' => "add");
        else
        return array('alert' => $result);
    }
    
    public function Edit($id, $text, $status, $edit)
    {   
        $text = $this->DataBase()->real_escape_string($text);
        $result=$this->DataBase()->query("UPDATE `tasks` SET `text`='$text', `status`=$status, `edit`=$edit WHERE `id`=$id");
        
        if($result)
        return array('alert' => "edit");
        else
        return array('alert' => $result);
    }
    
    public function setStatus($id)
    {   
        $this->DataBase()->query("UPDATE `tasks` SET `status`=1 WHERE `id`=$id");
    }
    
    public function Delete($id)
    {
        $this->DataBase()->query("DELETE FROM `tasks` WHERE `id`=$id");
    }
}
?>