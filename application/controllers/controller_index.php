<?php

class Controller_index extends Controller {
 
    function __construct() 
    {
        $this->model = new Model_index();
        $this->view = new View();
    }
    
    public function Action_index($params) 
    {
        $tasksOnPage = 3;
        
        if(empty($params['page']) || !is_numeric($params['page']))
        $activePage = 1;
        else
        $activePage = round($params['page']);
        
        if($params['sort']=="desc")
        $sort = "desc";
        else 
        $sort = "asc";
        
        switch($params['key'])
        {
            case "name": $key = "name";
            break;
            case "email": $key = "email";
            break;
            case "status": $key = "status";
            break;
            default: $key = "id";
            break;
        }
         
        $tasksCount = $this->model->getTasksCount();
        $pageCount = ceil($tasksCount/$tasksOnPage);
        
        if($activePage>0 && $activePage<=$pageCount)
        { 
            $data = $this->model->getPage($activePage, $tasksOnPage, $key, $sort);
            
            $this->view->generate('index', array('tasks'=>$data, 'pageCount'=>$pageCount, 'activePage'=>$activePage, 'tasksCount'=>$tasksCount, 'key'=>$key, 'sort'=>$sort));     
        }
        else
        {
            Route::RedirectTo();
        } 
    }
    
    public function Action_showAdd() 
    {
        $this->view->generate('add_task');
    }
    
    public function Action_add()
    {
        if(isset($_POST['name']))
        $name=htmlspecialchars($_POST['name']);
        
        if(isset($_POST['email']))
        $email=htmlspecialchars($_POST['email']);
        
        if(isset($_POST['text']))
        $text=htmlspecialchars($_POST['text']);
        
        if(filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)===false)
        $this->view->generate('add_task', array('alert' => "bad email", 'name'=>$name, 'email'=>$email, 'text'=>$text)); 
        else
        $this->view->generate('add_task', $this->model->Add($name, $email, $text));   
    }
    
    
    public function Action_showEdit($params) 
    {
        if(Session::get('admin')==true)
        $this->view->generate('edit_task', $this->model->getTask($params['id'])); 
        else
        Route::RedirectTo("user");
    }
    
    public function Action_edit() 
    {
        if(isset($_POST['id']))
        $id=$_POST['id'];
        
        $oldText = $_POST['oldtext'];
        $oldEdit = $_POST['edit'];
        
        if(isset($_POST['text']))
        $text=htmlspecialchars($_POST['text']);
        
        if($text!=$oldText || $oldEdit==1)
        $edit = 1;
        else 
        $edit = 0;
        
        if(isset($_POST['status']) && $_POST['status']!="")
        $status=1;
        else 
        $status=0;
        
        if(Session::get('admin')==true)
        {
            $this->model->Edit($id, $text, $status,$edit);
            Route::RedirectTo();
        }
        else
            Route::RedirectTo("user");
    }
    
    public function Action_setStatus($params)
    {
       if(Session::get('admin')==true)
        {
            $this->model->setStatus($params['id']);
            Route::RedirectTo();
        }
        else      
            Route::RedirectTo("user"); 
    }
    
    public function Action_delete($params) 
    {
        if(Session::get('admin')==true)
        {
            $this->model->Delete($params['id']);
            Route::RedirectTo();
        }
        else      
            Route::RedirectTo("user");
    }

}
?>