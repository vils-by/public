<?php

class Controller_user extends Controller{
 
    function __construct()
    {
        $this->view = new View();   
    }
    
    public function Action_index() 
    {
        Session::init();
        if(Session::get('admin') == false)
        {
           Session::destroy();
           $this->view->generate('login'); 
        }         
    }
    
    public function Action_login() 
    {
        Session::init();
        if($_POST['login']==ADMIN_NAME && $_POST['password']==ADMIN_PASS)
        {
            Session::set('admin', true);
            Route::RedirectTo();
        }
        else
         $this->view->generate('login', array('alert'=>"bad login", 'login'=>$_POST['login']));
    }
    
    public function Action_logout() 
    {
        Session::destroy();
        Route::RedirectTo();     
    }
}
?>