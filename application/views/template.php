<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<title>Test Task</title>

<link href="/css/bootstrap.css" rel="stylesheet">
<link href="/css/style.css" rel="stylesheet">

</head>
<body>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
<div class="container">
<div class="navbar-header">
<a class="navbar-brand" href="/">Задачи</a>
<a class="navbar-brand" href="/index/showadd">Добавить задачу</a>
<?=(Session::get('admin')==true?"<a class=\"navbar-brand\" href=\"/user/logout\">Выйти</a>"
    :"<a class=\"navbar-brand\" href=\"/user/\">Авторизация</a>")?>
</div>
</div>
</div>

<div class="container well margin-top30">
<?php      
include Q_PATH.'/application/views/view_'.$view.'.php';
?>
</div> 

<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
</body>
</html>