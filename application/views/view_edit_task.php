<h2>Редактирование задачи</h2>
<?if($alert=="edit") 
    echo "<div class=\"alert alert-success\">Задача отредактирована. <a href=\"/\">Все задачи</a></div>";?>
<form method="POST" action="/index/edit">
        <input type="hidden" name="id" value="<?=$id?>"/>
    <div class="form-group">
        <label>Имя пользователя</label>
        <input type="text" name="name" class="form-control" readonly="readonly" value="<?=$name?>"/>
    </div>
    <div class="form-group">
        <label>E-mail</label>
        <input type="email" name="email" class="form-control" readonly="readonly" value="<?=$email?>"/>
    </div>
    <div class="form-group">
        <label>Текст задачи</label>
        <input type="text" name="text" class="form-control" required="required" value="<?=$text?>"/>
        <input type="hidden" name="oldtext" value="<?=$text?>"/>
    </div>
    <div class="form-group">
        <label>
        <input type="checkbox" name="status" <?=$status==1 ? "checked=\"checked\"" : ""?>"/>
         Выполнено</label>  
    </div>
    <input type="hidden" name="edit" value="<?=$edit?>"/>
    <button type="submit" class="btn btn-block btn-primary">Сохранить</button>
</form>