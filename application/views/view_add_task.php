<h2>Добавление задачи</h2>
<?if($alert=="bad email") 
    echo "<div class=\"alert alert-danger\">Некорректный E-mail</div>";
 elseif($alert=="add") 
    echo "<div class=\"alert alert-success\">Задача добавлена. <a href=\"/\">Все задачи</a></div>";?>
<form method="POST" action="/index/add">
    <div class="form-group">
        <label>Имя пользователя</label>
        <input type="text" name="name" class="form-control" required="required" value="<?=$name?>"/>
    </div>
    <div class="form-group">
        <label>E-mail</label>
        <input type="text" name="email" class="form-control" required="required" value="<?=$email?>"/>
    </div>
    <div class="form-group">
        <label>Текст задачи</label>
        <input type="text" name="text" class="form-control" required="required" value="<?=$text?>"/>
    </div>
    <button type="submit" class="btn btn-block btn-primary">Добавить</button>
</form>