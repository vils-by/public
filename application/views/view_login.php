<div style="max-width: 350px; margin: 0 auto;">
<h2>Авторизация</h2>
<?if($alert=="bad login") 
    echo "<div class=\"alert alert-danger\">Некорректное имя пользователя или пароль</div>";?>
<form method="POST" action="/user/login">
    <label>Имя пользователя</label>
    <input type="text" name="login" class="form-control" required="required" value="<?=$login?>"/>
    <label>Пароль</label>
    <input type="password" name="password" class="form-control" required="required"/>
    <label class="checkbox margin-left25">
        <input type="checkbox" value="remember-me"/> Запомнить
    </label>
    <button class="btn btn-success btn-lg btn-block" type="submit">Войти</button>
</form>
</div>