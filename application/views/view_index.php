<h1>Задачи</h1>
<table class="table table-bordered">
    <tr>
        <th>ID</th>
        <th>
        <a href="/index/index/page/<?=$activePage."/key/name/sort/".($key=="name" && $sort=="asc"?"desc":"asc")?>">
        <?if($key=="name") 
        echo "<i class=\"glyphicon glyphicon-chevron-".($sort=="asc"?"down":"up")."\"></i> ";?> Имя&nbsp;пользователя</a>
        </th>
        <th>
        <a href="/index/index/page/<?=$activePage."/key/email/sort/".($key=="email" && $sort=="asc"?"desc":"asc")?>">
        <?if($key=="email") 
        echo "<i class=\"glyphicon glyphicon-chevron-".($sort=="asc"?"down":"up")."\"></i> ";?> E-mail</a>
        </th>
        <th>Текст задачи</th>
        <th>
        <a href="/index/index/page/<?=$activePage."/key/status/sort/".($key=="status" && $sort=="asc"?"desc":"asc")?>">
        <?if($key=="status") 
        echo "<i class=\"glyphicon glyphicon-chevron-".($sort=="asc"?"down":"up")."\"></i> ";?> Статус</a>
        </th>
    </tr>

<?foreach($tasks as $akey => $value):?>
    <tr>
        <td><?=$value['id']?></td>
        <td><?=$value['name']?></td>
        <td><?=$value['email']?></td>
        <td><?=$value['text']?></td>
        <td><?=($value['status']==1?"Выполнено":"")?>  <?=($value['edit']==1?"Отредактировано&nbsp;администратором":"")?></td>
<?if(Session::get('admin')==true) 
{
    echo "<td width=\"70px\">";
    if($value['status']==0)
        echo "<a href=\"/index/setstatus/id/".$value['id']."\" title=\"Выполнено\"><i class=\"glyphicon glyphicon-ok\"></i><a/> ";
    echo "<a href=\"/index/showedit/id/".$value['id']."\"  title=\"Изменить\"><i class=\"glyphicon glyphicon-edit\"></i><a/> 
        <a href=\"/index/delete/id/".$value['id']."\"  title=\"Удалить\" onclick=\"return confirm('Удалить задачу?')?true:false;\"><i class=\"glyphicon glyphicon-trash\"></i><a/>
    </td>";
}?>       
    </tr>
<?endforeach;?>
</table>
<?for($i=1; $i<=$pageCount; $i++)
{?>
<a href="/index/index/page/<?=$i."/key/".$key."/sort/".$sort?>" 
<?if($i==$activePage) 
    echo "class=\"page-number\"";?>>
<?=$i?></a>&nbsp;&nbsp;
<?} ?>
