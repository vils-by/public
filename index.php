<?php

ini_set('display_errors', 1);

define("Q_PATH", dirname(__FILE__));
define('DB_HOST', 'localhost');
define('DB_NAME', 'test');
define('DB_USER', 'root');
define('DB_PASS', '');
define('ADMIN_NAME', 'admin');
define('ADMIN_PASS', '123');

include Q_PATH.'/application/core/controller.php';
include Q_PATH.'/application/core/model.php';
include Q_PATH.'/application/core/view.php';

include Q_PATH.'/application/core/session.php';
Session::init();

include Q_PATH.'/application/core/route.php';
Route::Start();

?>